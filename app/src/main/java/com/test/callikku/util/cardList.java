package com.test.callikku.util;

/**
 * Created by golfyzzz on 12/13/2016.
 */

public class cardList {
    private String title, image;

    public cardList(String title,String image) {
        this.title = title;
        this.image = image;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}