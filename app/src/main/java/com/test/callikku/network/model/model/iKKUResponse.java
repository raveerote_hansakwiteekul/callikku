package com.test.callikku.network.model.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by golfyzzz on 12/13/2016.
 */

public class iKKUResponse {
    @SerializedName("activities") public List<ActivityList> activities;

    public List<ActivityList> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivityList> activities) {
        this.activities = activities;
    }
}
