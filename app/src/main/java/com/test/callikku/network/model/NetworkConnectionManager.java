package com.test.callikku.network.model;

import com.squareup.okhttp.ResponseBody;
import com.test.callikku.network.model.api.iKKUService;
import com.test.callikku.network.model.callback.onCalliKKUResponse;
import com.test.callikku.network.model.model.iKKUResponse;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by golfyzzz on 12/13/2016.
 */

public class NetworkConnectionManager {

    public NetworkConnectionManager() {

    }

    public void calliKKU(final onCalliKKUResponse listener){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.URL_SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        iKKUService iKKUService = retrofit.create(iKKUService.class);
        Call call = iKKUService.getData();
        call.enqueue(new Callback<iKKUResponse>() {
            @Override
            public void onResponse(Response<iKKUResponse> response, Retrofit retrofit) {
                iKKUResponse ikkuResponse = response.body();

                if(ikkuResponse == null) {
                    ResponseBody responseBody = response.errorBody();
                    if(responseBody != null) {
                        listener.onBodyError(responseBody);
                    } else {
                        listener.onBodyErrorIsNull();
                    }
                } else {
                    listener.onResponse(ikkuResponse, retrofit);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onFailure(t);
            }
        });

    }

}
