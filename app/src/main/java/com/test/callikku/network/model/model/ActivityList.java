package com.test.callikku.network.model.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by golfyzzz on 12/13/2016.
 */

public class ActivityList {
    @SerializedName("title") public String title;
    @SerializedName("content") public String content;
    @SerializedName("image") public String image;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
