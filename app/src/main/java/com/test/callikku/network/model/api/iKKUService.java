package com.test.callikku.network.model.api;

import com.test.callikku.network.model.model.iKKUResponse;

import retrofit.Call;
import retrofit.http.POST;

/**
 * Created by golfyzzz on 12/13/2016.
 */

public interface iKKUService {

    @POST("services/topActivity")
    Call<iKKUResponse> getData();

}
