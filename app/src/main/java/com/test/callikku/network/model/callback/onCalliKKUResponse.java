package com.test.callikku.network.model.callback;

import com.squareup.okhttp.ResponseBody;
import com.test.callikku.network.model.model.iKKUResponse;

import retrofit.Retrofit;

/**
 * Created by golfyzzz on 12/13/2016.
 */

public interface onCalliKKUResponse {
    void onResponse(iKKUResponse iKKUResponse, Retrofit retrofit);
    void onBodyError(ResponseBody responseBodyError);
    void onBodyErrorIsNull();
    void onFailure(Throwable t);
}
