package com.test.callikku.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.test.callikku.R;
import com.test.callikku.util.cardList;

import java.util.List;

/**
 * Created by golfyzzz on 12/13/2016.
 */

public class iKKUAdapter extends RecyclerView.Adapter<iKKUAdapter.MyViewHolder> {

    private List<cardList> init_cardList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView title;
        public AppCompatImageView image;


        public MyViewHolder(View view) {
            super(view);
            title = (AppCompatTextView) view.findViewById(R.id.title);
            image = (AppCompatImageView) view.findViewById(R.id.image);

        }
    }

    public iKKUAdapter(List<cardList> init_cardList) {
        this.init_cardList = init_cardList;
    }

    @Override
    public iKKUAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpater_ikku, parent, false);

        context = parent.getContext();


        return new iKKUAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final iKKUAdapter.MyViewHolder holder, final int position) {
        final cardList card = init_cardList.get(position);
        holder.title.setText(card.getTitle());

        Glide.with(context)
                .load(card.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);



    }

    @Override
    public int getItemCount() {
        return init_cardList.size();
    }


}

