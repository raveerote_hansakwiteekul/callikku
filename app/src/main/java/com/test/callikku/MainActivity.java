package com.test.callikku;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.squareup.okhttp.ResponseBody;
import com.test.callikku.adapter.iKKUAdapter;
import com.test.callikku.network.model.NetworkConnectionManager;
import com.test.callikku.network.model.callback.onCalliKKUResponse;
import com.test.callikku.network.model.model.ActivityList;
import com.test.callikku.network.model.model.iKKUResponse;
import com.test.callikku.util.cardList;

import java.util.ArrayList;
import java.util.List;

import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    private RecyclerView recyclerView;
    private List<cardList> init_cardList = new ArrayList<>();
    private iKKUAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.abc);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        new NetworkConnectionManager().calliKKU(calliKKUResponse);
    }

    onCalliKKUResponse calliKKUResponse = new onCalliKKUResponse() {

        @Override
        public void onResponse(iKKUResponse iKKUResponse, Retrofit retrofit) {
            RecyclerView.LayoutManager mLayoutManager =
                    new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);

            mAdapter = new iKKUAdapter(init_cardList);

            for (int i = 0 ; i< iKKUResponse.activities.size() ; i++){
                ActivityList kkuResponse = iKKUResponse.activities.get(i);
                cardList card = new cardList(kkuResponse.title, kkuResponse.image);
                init_cardList.add(card);

            }

            recyclerView.setAdapter(mAdapter);
            //textView.setText(iKKUResponse.activities.get(0).getTitle());

        }

        @Override
        public void onBodyError(ResponseBody responseBodyError) {

        }

        @Override
        public void onBodyErrorIsNull() {

        }

        @Override
        public void onFailure(Throwable t) {

        }
    };
}
